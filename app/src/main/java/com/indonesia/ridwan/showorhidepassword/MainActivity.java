package com.indonesia.ridwan.showorhidepassword;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText ed = (EditText) findViewById(R.id.edittxt1);
        CheckBox c = (CheckBox) findViewById(R.id.checkbox1);

        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (!isChecked){

                    ed.setTransformationMethod(PasswordTransformationMethod.getInstance());

                } else {

                    ed.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });


    }
}
